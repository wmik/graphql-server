const debug = require('debug')('app:database');
module.exports = {
  test: {
    client: 'pg',
    connection: {
      host: process.env.TEST_ENV === 'ci' ? 'postgres' : 'localhost',
      user: 'test',
      password: 'test',
      database: 'test'
    },
    migrations: {
      directory: '../database/migrations'
    },
    seeds: {
      directory: '../database/seeds'
    }
  }
};
