const debug = require('debug')('app:knex');
const knexConfig = require('./knex.config');

module.exports = {
  graphql: {},
  jwt: {
    JWT_SECRET: 'supersecret',
    EXPIRY: Math.floor(new Date().getTime() / 1000) + 7 * 24 * 60 * 60
  },
  knex: Object.assign({}, knexConfig.test, {
    debug: Boolean(process.env.DEBUG),
    log: {
      warn(message) {
        console.warn('app:database:warn', message);
      },
      error(message) {
        console.error('app:database:error', message);
      },
      debug(message) {
        debug(message);
      }
    },
    migrations: {
      directory: '../database/migrations'
    },
    seeds: {
      directory: '../database/seeds'
    }
  })
};
