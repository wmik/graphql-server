exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('authentication', function(table) {
      table.increments('user_id').primary();
      table.string('username').notNullable();
      table.string('password').notNullable();
      table.jsonb('roles').notNullable();
    })
    .createTable('candidates', function(table) {
      table.increments('candidate_id').primary();
      table.string('first_name').notNullable();
      table.string('last_name').notNullable();
      table.string('email').notNullable();
      table.string('phone').notNullable();
      table.string('city').notNullable();
      table.string('state');
      table.string('country').notNullable();
      table.string('industry').notNullable();
      table.string('job_title').notNullable();
      table.integer('years_of_experience').notNullable();
      table.jsonb('hard_skills').notNullable();
      table.jsonb('domain').notNullable();
      table.jsonb('social_media_urls');
      table.timestamps(true, true);
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTableIfExists('authentication')
    .dropTableIfExists('candidates');
};
