const data = require('./data');

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return Promise.join(
    knex('authentication').del(),
    knex('authentication').insert(data.authentication),
    knex('candidates').del(),
    knex('candidates').insert(data.candidates)
  );
};
