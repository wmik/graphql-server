import test from 'ava';
import { has, hasAllKeys, isConnection, isError } from '.';

test('it should return true for existing keys', t => {
  const object = { a: 1 };
  t.true(has(object, 'a'));
  t.true(hasAllKeys(object, 'a'));
  t.true(isConnection({ totalCount: 1, edges: [], pageInfo: {} }));
  t.true(isError({ errors: [] }));
});

test('it should return false for missing keys', t => {
  const object = { a: 1 };
  t.false(has(object, 'b'));
  t.false(hasAllKeys(object, 'a', 'b'));
  t.false(isConnection({}));
  t.false(isError({}));
});
