function has(object, key) {
  return Object.hasOwnProperty.call(object, key);
}

function hasAllKeys(object, ...keys) {
  return keys.every(key => has(object, key));
}

function isConnection(object) {
  return hasAllKeys(object, 'totalCount', 'pageInfo', 'edges');
}

function isError(object) {
  return has(object, 'errors');
}

module.exports = { has, hasAllKeys, isConnection, isError };
