import test from 'ava';
import { createFragment, createRelayConnectionFragment } from './query';

test('it should create a graphql fragment with only top level fields given valid arguments', t => {
  const fragment = createFragment('userFragment', 'User', ['id', 'firstName']);
  t.is(fragment, 'fragment userFragment on User { id firstName }');
});

test('it should create a graphql fragment with nested fields given valid arguments', t => {
  const fragment = createFragment(
    'usersConnectionFragment',
    'UsersConnection',
    [
      'totalCount',
      { name: 'pageInfo', fields: ['hasNextPage'] },
      { name: 'edges', fields: [{ name: 'node', fields: ['id'] }] }
    ]
  );
  t.is(
    fragment,
    'fragment usersConnectionFragment on UsersConnection { totalCount pageInfo { hasNextPage } edges { node { id } } }'
  );
});

test('it should create a graphql relay connection fragment given valid arguments', t => {
  const fragment = createRelayConnectionFragment(
    'usersConnectionFragment',
    'UsersConnection',
    'users'
  );
  t.is(
    fragment,
    'fragment usersConnectionFragment on UsersConnection { totalCount pageInfo { hasNextPage hasPreviousPage startCursor endCursor } edges { cursor node { id } } users }'
  );
});
