function createFields(array) {
  return array
    .map(field => {
      if (typeof field === 'string') {
        return field;
      }
      const { name, fields } = field;
      return `${name} { ${createFields(fields)} }`;
    })
    .join(' ');
}

function createFragment(fragmentName, on, ...fields) {
  fields = Array.isArray(fields[0])
    ? fields[0].concat(fields.slice(1))
    : fields;
  return `fragment ${fragmentName} on ${on} { ${createFields(fields)} }`;
}

function createRelayConnectionFragment(fragmentName, on, ...fields) {
  return createFragment(
    fragmentName,
    on,
    [
      'totalCount',
      {
        name: 'pageInfo',
        fields: ['hasNextPage', 'hasPreviousPage', 'startCursor', 'endCursor']
      },
      {
        name: 'edges',
        fields: ['cursor', { name: 'node', fields: ['id'] }]
      }
    ].concat(fields)
  );
}

module.exports = {
  createRelayConnectionFragment,
  createFragment,
  createFields
};
