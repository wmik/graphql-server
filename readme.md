> # graphql-server

Built with

|Technology|Version|
|-|-|
|*GraphQL*|14.3.0|
|*Apollo Server*|2.5.0|
|*Relay (graphql-relay)*|0.6.0|
|*JWT*|8.5.1|
|*PostgreSQL*|9.5.0|
|*Knex*|0.16.5|
|*BcryptJS*|2.4.3|

## Installation and Setup
1. Clone the repository

        git clone <repo_url>

2. Install the dependencies

        npm install

3. Create a `.env` file and copy the contents of `.env.copy` and set the provided environment variables

    *On Unix:*
      
        cp .env.copy .env

4. Start the server

        npm start


## License
MIT &copy; 2019
