const { verify } = require('jsonwebtoken');
const { JWT_SECRET } = require('config').get('jwt');

module.exports = ({ req }) => {
  const token = req ? req.headers.authorization : '';
  let user = false;
  try {
    user = verify(token, JWT_SECRET);
  } catch (error) {
    user = false;
  }
  return { user };
};
