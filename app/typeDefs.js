const { importSchema } = require('graphql-import');
const typeDefs = importSchema('app/schema.graphql');
module.exports = typeDefs;
