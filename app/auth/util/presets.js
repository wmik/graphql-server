const {
  createFragment,
  createRelayConnectionFragment
} = require('../../../util/query');
const { ERRORS_FRAGMENT } = require('../../error/util/presets').fragments;

const USERS_CONNECTION_FRAGMENT = createRelayConnectionFragment(
  'userConnectionFragment',
  'UsersConnection',
  'users'
);

const USER_FRAGMENT = createFragment('userFragment', 'User', ['id']);

const USER_LOGIN = `
  query userLogin($username: String!, $password: String!) {
    login(username: $username, password: $password)
  }
`;

const GET_MANY_USERS = `
query getManyUsers {
  allUsers {
    ...usersConnectionFragment
    ...errorsFragment
  }
}
${USERS_CONNECTION_FRAGMENT}
${ERRORS_FRAGMENT}
`;

const GET_SINGLE_USER = `
query getSingleUser($userId: String!) {
  userById(userId: $userId) {
    ...userFragment
    ...errorsFragment
  }
}
${USER_FRAGMENT}
${ERRORS_FRAGMENT}
`;

module.exports = {
  query: {
    USER_LOGIN,
    GET_SINGLE_USER,
    GET_MANY_USERS
  }
};
