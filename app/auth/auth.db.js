const { SQLDataSource } = require('datasource-sql');
const { compare } = require('bcryptjs');
const { sign } = require('jsonwebtoken');
const { JWT_SECRET, EXPIRY } = require('config').get('jwt');

function generateToken(user) {
  return sign(
    {
      username: user.username,
      roles: user.roles,
      exp: EXPIRY
    },
    JWT_SECRET
  );
}

class AuthDB extends SQLDataSource {
  constructor(knex) {
    super();
    this.knex = knex;
  }

  async getUsers() {
    const users = this.db.select().from('authentication');
    return users;
  }

  async getUserById(id) {
    const [user] = await this.db
      .select()
      .from('authentication')
      .where('user_id', id);
    return user;
  }

  async login(username, password) {
    const [user] = await this.db
      .select()
      .from('authentication')
      .where('username', username);
    let loggedIn = false;
    if (user) {
      loggedIn = await compare(password, user.password);
    }
    if (loggedIn) {
      const token = generateToken(user);
      return { token };
    }
  }
}

module.exports = AuthDB;
