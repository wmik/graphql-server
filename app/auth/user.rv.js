const { toGlobalId } = require('graphql-relay');
const { has, isConnection, isError } = require('../../util');

function isUser(object) {
  return has(object, 'user_id');
}

const User = {
  id: data => toGlobalId('user', data.user_id)
};

const SingleUserResponse = {
  __resolveType: object => {
    if (isUser(object)) {
      return 'User';
    }
    if (isError(object)) {
      return 'Errors';
    }
  }
};

const ManyUsersResponse = {
  __resolveType: object => {
    if (isConnection(object)) {
      return 'UsersConnection';
    }
    if (isError(object)) {
      return 'Errors';
    }
  }
};

module.exports = {
  User,
  ManyUsersResponse,
  SingleUserResponse
};
