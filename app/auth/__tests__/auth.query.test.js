import test from 'ava';
import { createTestClient } from 'apollo-server-testing';
import server from '../../';
import presets from '../util/presets';

const { USER_LOGIN } = presets.query;

test('it should return a token given valid login credentials', async t => {
  const { query } = createTestClient(server);
  const res = await query({
    query: USER_LOGIN,
    variables: { username: 'admin', password: 'pass123' }
  });
  t.true('token' in res.data.login);
});

test('it should return an error object given invalid login credentials', async t => {
  const { query } = createTestClient(server);
  const res = await query({
    query: USER_LOGIN,
    variables: { username: 'admin', password: 'pass12' }
  });
  t.false('token' in res.data.login);
  t.true('errors' in res.data.login);
  t.deepEqual(res.data.login.errors, [
    { message: 'invalid login credentials', path: ['login'] }
  ]);
});
