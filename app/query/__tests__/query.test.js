import test from 'ava';
import { createTestClient } from 'apollo-server-testing';
import server from '../../';

import presets from '../util/presets';
const { GET_STATUS } = presets.query;

test('it should return `ok` status', async t => {
  const { query } = createTestClient(server);
  const res = await query({ query: GET_STATUS });
  t.deepEqual(res.data, { status: 'ok' });
});
