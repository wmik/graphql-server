const GET_STATUS = `query getStatus { status }`;

module.exports = {
  query: {
    GET_STATUS
  }
};
