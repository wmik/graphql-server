const { connectionFromArray, fromGlobalId } = require('graphql-relay');

function withField(object, key, value) {
  return Object.assign({}, object, { [key]: value });
}

function withTotalCount(connection) {
  return withField(connection, 'totalCount', connection.edges.length);
}

function createError(message, path) {
  path = Array.isArray(path) ? path : [path];
  return {
    errors: [{ message, path }]
  };
}

function authFail(path) {
  return createError('unauthorized', path);
}

module.exports = {
  status: () => 'ok',
  allCandidates: async (_, args, { dataSources, user }) => {
    if (!user) {
      return authFail('allCandidates');
    }
    const candidates = await dataSources.candidates.getCandidates();
    const connection = connectionFromArray(candidates, args);
    return withField(withTotalCount(connection), 'candidates', candidates);
  },
  candidateById: (_, args, { dataSources, user }) => {
    if (!user) {
      return authFail('candidateById');
    }
    return dataSources.candidates.getCandidateById(args.candidateId);
  },
  searchCandidates: async (_, args, { dataSources, user }) => {
    if (!user) {
      return authFail('searchCandidates');
    }
    const candidates = await dataSources.candidates.searchCandidates(args);
    const connection = connectionFromArray(candidates, args);
    return withField(withTotalCount(connection), 'candidates', candidates);
  },
  allUsers: async (_, args, { dataSources, user }) => {
    if (!(user && user.roles.includes('admin'))) {
      return authFail('allUsers');
    }
    const users = await dataSources.auth.getUsers();
    const connection = connectionFromArray(users, args);
    return withField(withTotalCount(connection), 'users', users);
  },
  userById: (_, args, { dataSources, user }) => {
    if (!user) {
      return authFail('userById');
    }
    return dataSources.auth.getUserById(args.userId);
  },
  login: async (_, args, { dataSources }) => {
    const result = await dataSources.auth.login(args.username, args.password);
    if (!result) {
      return createError('invalid login credentials', 'login');
    }
    return result;
  },
  Node: (_, args, { dataSources, user }) => {
    if (!user) {
      return authFail('Node');
    }
    const { type, id } = fromGlobalId(args.id);
    switch (type) {
      case 'candidate':
        return dataSources.candidates.getCandidateById(id);
      case 'user':
        return dataSources.auth.getUserById(id);
      default:
        return null;
    }
  }
};
