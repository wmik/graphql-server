const { createFragment } = require('../../../util/query');
const { ERRORS_FRAGMENT } = require('../../error/util/presets').fragments;

const GET_MANY_CANDIDATES = `
query getManyCandidates {
  allCandidates {
    ...candidateConnectionFragment
    ...errorsFragment
  }
}
${CANDIDATES_CONNECTION_FRAGMENT}
${ERRORS_FRAGMENT}
`;

const GET_SINGLE_CANDIDATE = `
query getSingleCandidate($candidateId: String!) {
  candidateById(candidateId: $candidateId) {
    ...candidateFragment
    ...errorsFragment	
  }
}
${CANDIDATE_FRAGMENT}
${ERRORS_FRAGMENT}
`;

const SEARCH_CANDIDATES = `
query searchCandidates($fields: JSON) {
  searchCandidates(fields: $fields) {
    ...candidatesConnectionFragment
    ...errorsFragment
  }
}
${CANDIDATES_CONNECTION_FRAGMENT}
${ERRORS_FRAGMENT}
`;

const CANDIDATE_FRAGMENT = createFragment('candidateFragment', 'Candidate', [
  'id'
]);

const CANDIDATES_CONNECTION_FRAGMENT = createRelayConnectionFragment(
  'candidateConnectionFragment',
  'CandidatesConnection',
  'candidates'
);

module.exports = {
  query: {
    GET_SINGLE_CANDIDATE,
    GET_MANY_CANDIDATES,
    SEARCH_CANDIDATES
  },
  fragments: {
    CANDIDATE_FRAGMENT,
    CANDIDATES_CONNECTION_FRAGMENT
  }
};
