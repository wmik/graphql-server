const { toGlobalId } = require('graphql-relay');
const { has, isConnection, isError } = require('../../util');

function isCandidate(object) {
  return has(object, 'candidate_id');
}

const Candidate = {
  id: data => toGlobalId('candidate', data.candidate_id),
  firstName: data => data.first_name,
  lastName: data => data.last_name,
  jobRole: data => data.job_role_title,
  hardSkills: data => data.hard_skills,
  socialMediaUrls: data => data.social_media_urls,
  yearsOfExperience: data => data.years_of_experience
};

const SingleCandidateResponse = {
  __resolveType: object => {
    if (isCandidate(object)) {
      return 'Candidate';
    }
    if (isError(object)) {
      return 'Errors';
    }
  }
};

const ManyCandidatesResponse = {
  __resolveType: object => {
    if (isConnection(object)) {
      return 'CandidatesConnection';
    }
    if (isError(object)) {
      return 'Errors';
    }
  }
};

module.exports = {
  Candidate,
  SingleCandidateResponse,
  ManyCandidatesResponse
};
