const { SQLDataSource } = require('datasource-sql');

/**
 * Converts a string into a logical operator symbol
 * @param {string} string
 * @param {string} fallback
 */
function getComparator(string, fallback = '=') {
  switch (string) {
    case 'gt':
      return '>';
    case 'lt':
      return '<';
    case 'jsonbin':
      return '@>';
    default:
      return fallback;
  }
}

/**
 * Converts a camelCase string to_snake_case
 * @param {string} string
 */
function toSnakeCase(string) {
  return string.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
}

class CandidatesDB extends SQLDataSource {
  constructor(knex) {
    super();
    this.knex = knex;
  }

  async getCandidates() {
    const candidates = await this.db.select().from('candidates');
    return candidates;
  }

  async getCandidateById(id) {
    const [candidate] = await this.db
      .select()
      .from('candidates')
      .where('candidate_id', id);
    return candidate;
  }

  async searchCandidates(args) {
    const bindings = [];
    const queries = Object.entries(args.fields).map(([key, value]) => {
      const normalized = toSnakeCase(key);
      if (typeof value === 'string') {
        bindings.push(value);
        return `${normalized} = ?`;
      }
      // TODO: Refactor to support multiple operators e.g. { gt: 1, lt: 3 }
      const [comparator] = Object.keys(value);
      let [val] = Object.values(value);
      if (typeof val !== 'string') {
        val = JSON.stringify(val);
      }
      bindings.push(val);
      return `${normalized} ${getComparator(comparator)} ?`;
    });
    return await this.db
      .select()
      .from('candidates')
      .whereRaw(queries.join(` ${args.operator || 'and'} `), bindings);
  }
}

module.exports = CandidatesDB;
