const { ApolloServer } = require('apollo-server');

const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const dataSources = require('./dataSources');
const context = require('./context');

const server = new ApolloServer({ typeDefs, resolvers, dataSources, context });

module.exports = server;
