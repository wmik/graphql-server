const { USER_FRAGMENT } = require('../../auth/util/presets').fragments;
const {
  CANDIDATE_FRAGMENT
} = require('../../candidate/util/presets').fragments;
const { ERRORS_FRAGMENT } = require('../../error/util/presets').fragments;

const GET_NODE = `
query getNode($id: String!) {
  Node(id: $id) {
    ...candidateFragment
    ...userFragment
    ...errorsFragment
  }
}
${CANDIDATE_FRAGMENT}
${USER_FRAGMENT}
${ERRORS_FRAGMENT}
`;

module.exports = {
  query: {
    GET_NODE
  }
};
