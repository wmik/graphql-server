function has(object, key) {
  return Object.hasOwnProperty.call(object, key);
}

function hasAllKeys(object, ...keys) {
  return keys.every(key => has(object, key));
}

function isCandidate(object) {
  return hasAllKeys(object, 'first_name', 'last_name', 'email');
}

function isUser(object) {
  return hasAllKeys(object, 'username', 'roles');
}

module.exports = {
  __resolveType: object => {
    if (isCandidate(object)) {
      return 'Candidate';
    }
    if (isUser(object)) {
      return 'User';
    }
  }
};
