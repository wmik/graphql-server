const Knex = require('knex');
const knexConfig = require('config').get('knex');
const AuthDB = require('./auth/auth.db');
const CandidatesDB = require('./candidate/candidate.db');

const knex = Knex(knexConfig);

module.exports = () => ({
  auth: new AuthDB(knex),
  candidates: new CandidatesDB(knex)
});
