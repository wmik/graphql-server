const { createFragment } = require('../../../util/query');

const ERRORS_FRAGMENT = createFragment('errorFragment', 'Errors', [
  { name: 'errors', fields: ['message', 'path'] }
]);

module.exports = {
  fragments: {
    ERRORS_FRAGMENT
  }
};
