module.exports = {
  User: require('./auth/user.rv').User,
  SingleUserResponse: require('./auth/user.rv').SingleUserResponse,
  ManyUsersResponse: require('./auth/user.rv').ManyUsersResponse,
  Candidate: require('./candidate/candidate.rv').Candidate,
  SingleCandidateResponse: require('./candidate/candidate.rv')
    .SingleCandidateResponse,
  ManyCandidatesResponse: require('./candidate/candidate.rv')
    .ManyCandidatesResponse,
  Node: require('./node/node.rv'),
  Query: require('./query/query.rv')
};
